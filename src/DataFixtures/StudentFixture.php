<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Student;

class StudentFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($x = 1; $x < 10; $x++) {

        $student = new Student();
        $student->setName("name $x");
        $student->setLevel($x);
        $student->setTech(["Symfony", "javascript"]);
        $manager->persist($student);
        }

        $manager->flush();
    }
}
