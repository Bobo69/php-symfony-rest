<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Student;
use Symfony\Component\Serializer\SerializerInterface;
use App\Repository\StudentRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Form\StudentType;
use Symfony\Component\Form\FormErrorIterator;

    /**
     * @Route("/rset/student", name="rset_student")             #route principale
     */

class RsetStudentController extends Controller
{
    private $serializer;
    public function  __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods={"GET"})                             #ont peut acceder a la methode de cette route que par la requete GET 
     */

    public function index(/*SerializerInterface $serializer,*/ StudentRepository $repo) #repo chercher sur la DB
    {
        // $response = new Response();
        // $response->headers->set("Content-type", "application/json");
        // $response->setStatusCode(200);
        // $response->setContent(json_encode(["ga" => "bu"]));
        // return $response;

        // return new JsonResponse(["gab" => "bu"]);

        // $student = new Student();
        // $student->setName("test");
        // $student->setLevel(5);
        // $student->setTech(["Symfony", "javascript"]);

        // return new JsonResponse($student);
        #-----------------------------------------------------------------

        // $json = $serializer->serialize($student, "json");
        // return JsonResponse::fromJsonString($json);


        #-----------------------------------------------------------------
        $repo = $this->getDoctrine()->getRepository(Student::class);
        $json = $this->serializer->serialize($repo->findAll(), "json");               #$repo->findAll() va chercher tt les dooneés (Studet) sur la DB;                                                                                                                               Transformation en instance 
        return JsonResponse::fromJsonString($json);
    }
    /**
     * @Route("/", methods={"POST"})
     */

    public function add(Request $req) {
        $form = $this->createForm(StudentType::class);

        $form->submit(json_decode($req->getContent(), true));

        if ($form->isSubmitted() && $form->isValid()) {
            $student = $form->getData();
            
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($student);
            $manager->flush();

            return new JsonResponse(["id" => $student->getId()]);
        }



        // $body = $req->getContent();             #getContent renvoie ce qu'il ya dans le BODY
        // $student = $this->serializer->deserialize($body, Student::class, "json");
        // // return new Response($student->getName() . $student->getLevel());
        // $manager = $this->getDoctrine()->getManager();
        // $manager->persist($student);
        // $manager->flush();

        // return new JsonResponse(["id" => $student->getId()]);
        return new JsonResponse(["errors" => $this->listFormErrors($form->getErrors())], 500);
    }
        
        

    /**
     * @Route("/{student}", methods="GET")
     */
    public function single(Student $student) {
        $json= $this->serializer->serialize($student, "json");
        return JsonResponse::fromJsonString(($json));
    }

    /**
     * @Route("/{student}", methods={"DELETE"})
     */
    public function remove(Student $student) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($student);
        $manager->flush();
        return new Response();
    }

    // /**
    //  * @Route("/{id}", methods={"PUT"})
    //  */
    // public function update(int $id, Request $req) {
    //     $body = $req->getContent();
    //     $student = $this->serializer->deserialize($body, Student::class, "json");
    //     $student->setId($id);

    //     $manager = $this->getDoctrine()->getManager();
    //     $manager->remove($student);
    //     $manager->flush();
    //     return new Response();
    
    // }

    /**
     * @Route("/{student}", methods={"PUT"})
     */
    public function update(Student $student, Request $req) {
        $body = $req->getContent();             #getContent renvoie ce qu'il ya dans le BODY
        $updated = $this->serializer->deserialize($body, Student::class, "json");

        $manager = $this->getDoctrine()->getManager();
        $student->setName($updated->getName());
        $student->setLevel($updated->getLevel());
        $student->setTech($updated->getTech());
        
        $manager->flush();
        return new Response();
    }
    private function listFormErrors(FormErrorIterator $errors) {
        $list = [];
        foreach($errors as $error) {
            $list[] = $error->getMessage();
        }
        return $list;
    }
}

